﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CryptClass
{
    internal class SHA3
    {
        public string Encrypt512(string input)
        {
            var hashAlgorithm = new Org.BouncyCastle.Crypto.Digests.Sha3Digest(512);

            // Choose correct encoding based on your usecase
            byte[] inputBytes = Encoding.ASCII.GetBytes(input);

            hashAlgorithm.BlockUpdate(inputBytes, 0, input.Length);

            byte[] result = new byte[64]; // 512 / 8 = 64
            hashAlgorithm.DoFinal(result, 0);

            string hashString = BitConverter.ToString(result);
            hashString = hashString.Replace("-", "").ToLowerInvariant();

            return hashString;
        }
    }
}
