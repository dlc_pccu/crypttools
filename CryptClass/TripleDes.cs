﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Web.Security;

namespace CryptClass
{
    // Online UUID Generator
    // https://www.uuidgenerator.net/version4

    // build ActiveX COM for classic asp
    // https://dotblogs.com.tw/regionbbs/2010/11/15/developing_active_x_control_with_cs

    [Guid("c6d91402-4d23-4628-b120-0b937c469acd")]
    [ComVisible(true)]
    public class TripleDes
    {
        // 加密鑰匙, 加密鑰匙長度需為 24 碼
        private string Salt { get => "DYEOk!WS@4HQrF1'MHGzbvki"; }

        // SessionId 用的 加密鑰匙, 加密鑰匙長度需為 24 碼
        private string SaltForSessionId { get => "iN0FZzxBLw0xHNhdaoWKmdUK"; }

        public string Input { get; set; }

        public string Output { get; set; }

        public string SessionId { get; set; }

        public string Encrypt()
        {
            string strKey = this.Salt;
            RandomString randomString = new RandomString();

            TripleDESCryptoServiceProvider DES = new TripleDESCryptoServiceProvider();
            DES.Key = UTF8Encoding.UTF8.GetBytes(FormsAuthentication.HashPasswordForStoringInConfigFile(strKey, "md5").Substring(0, 24));
            DES.Mode = CipherMode.ECB;

            ICryptoTransform DESEncrypt = DES.CreateEncryptor();

            this.Input = $"{EncryptSessionId(this.SessionId)}|||{randomString.GetRandomString()}|||{this.Input}|||{DateTime.UtcNow.ToLongTimeString()}";

            byte[] buffer = UTF8Encoding.UTF8.GetBytes(this.Input);

            this.Output = Convert.ToBase64String(DESEncrypt.TransformFinalBlock(buffer, 0, buffer.Length));
            return this.Output;
        }

        private string EncryptSessionId(string sessionId)
        {
            var totp = new TOTP();
            string strKey = this.SaltForSessionId;

            string input = $"{strKey}|||{totp.GetTotpCode(sessionId)}|||{sessionId}";

            string output = new SHA3().Encrypt512(input);
            return output;
        }

        public string Decrypt()
        {
            string strKey = this.Salt;

            TripleDESCryptoServiceProvider DES = new TripleDESCryptoServiceProvider();
            DES.Key = UTF8Encoding.UTF8.GetBytes(System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(strKey, "md5").Substring(0, 24));
            DES.Mode = CipherMode.ECB;
            DES.Padding = System.Security.Cryptography.PaddingMode.PKCS7;

            ICryptoTransform DESDecrypt = DES.CreateDecryptor();

            string result = String.Empty;

            byte[] Buffer = Convert.FromBase64String(this.Input);

            var decryptString = UTF8Encoding.UTF8.GetString(DESDecrypt.TransformFinalBlock(Buffer, 0, Buffer.Length));
            var splitDecryptString = decryptString.Split(new string[] { "|||" }, StringSplitOptions.None);

            this.Output = String.Empty;

            if (splitDecryptString.Length == 4)
            {
                var sessionKeyVerify = EncryptSessionId(this.SessionId);
                if (sessionKeyVerify.Equals(splitDecryptString[0], StringComparison.CurrentCulture))
                {
                    this.Output = splitDecryptString[2];
                }
                else
                {
                    this.Output = $"{sessionKeyVerify} &&& {splitDecryptString[0]}";
                }
            }

            return this.Output;
        }
    }
}
