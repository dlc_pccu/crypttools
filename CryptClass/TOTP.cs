﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OtpNet;

namespace CryptClass
{
    internal class TOTP
    {
        
        public string GetTotpCode(string secretKey)
        {
            // step = 60 : a new code will be generated every 60 seconds
            var totp = new Totp(UTF8Encoding.UTF8.GetBytes(secretKey), mode: OtpHashMode.Sha512, step: 60);

            // code calculation
            var totpCode = totp.ComputeTotp(DateTime.UtcNow);

            return totpCode;
        }

        public bool VerifyCode(string secretKey, string code)
        {
            var totp = new Totp(UTF8Encoding.UTF8.GetBytes(secretKey), mode: OtpHashMode.Sha512, step: 60);

            long timeStepMatched = 0;

            var result = totp.VerifyTotp(code, out timeStepMatched);

            return result;
        }
    }
}
